import React, { useState } from 'react';
import { Container, Box, Title, LeftSide, RightSide } from './styles';
import StoreCard from '../../components/StoreCard';
import stores from '../../lib/assets/mockData/stores.json'

const Stores = () => {
  const [storesData, setData] = useState(stores.data);
  const leftSide = []
  const rightSide = []

  function getWindowDimensions() {
    const { innerWidth: width} = window;
    return {
      width
    };
  }
  const screen = getWindowDimensions()

  storesData.map((store, index) => {
    if (index % 2 === 0) {
        leftSide.push(store)
        return leftSide
    } else {
        rightSide.push(store)
        return rightSide
    }
  })

  return (
    <Container>
      <Title>10 magasins</Title>
      <Box>
        {screen.width < 780 ? 
        <>
          <LeftSide>
            {leftSide.map(store =>
              <StoreCard key={store.id}
              name={store.storeName}
              image={store.imagePath}
              />
              )}
          </LeftSide>
          <RightSide>
            {rightSide.map(store =>
              <StoreCard key={store.id}
              name={store.storeName}
              image={store.imagePath}
              />
              )}
          </RightSide>
        </>
        :
        <LeftSide>
          {storesData.map(store =>
            <StoreCard key={store.id}
            name={store.storeName}
            image={store.imagePath}
            />
            )}
        </LeftSide>
          }
      </Box>
    </Container>
  );
}

export default Stores;