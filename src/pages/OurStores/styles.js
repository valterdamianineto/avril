import styled from "styled-components";

export const Container = styled.div`
    width: 100%;
`
export const Box = styled.div`
    width: 100%;
    display: flex;
`

export const Title = styled.p`
    color: #373D49;
    font-weight: 700;
    font-size: 24px;
    line-height: 28.8px;
`

export const LeftSide = styled.div`
    width: 100%;
    display: block;
    padding: 2px;
    align-items: top;
    @media (max-width: 768px) {
        width: 50%;
    }
`

export const RightSide = styled.div`
    width: 50%;
    padding: 2px;
`

