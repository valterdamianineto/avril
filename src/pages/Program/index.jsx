import React from 'react';
import { Page } from './styles';
import Card from '../../components/Card';
import program from '../../lib/assets/mockData/program.json';

export default class AboutAvril extends React.Component {
  render() {
    return (
      <Page>
        {program.data.map(card => {
          return (
            <Card title={card.title} p1={card.p1} p2={card.p2} p3={card.p3} text={card.btn}/>
            )
          })
        }
      </Page>
    );
  }
}