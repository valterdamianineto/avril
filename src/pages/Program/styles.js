import styled from "styled-components";

export const Page = styled.div`
    background: #EDEFDB;
    padding: 32px 16px;
    display: flex;
    justify-content: center;
    margin: 20px auto;
    @media (max-width: 768px) {
        display: block;
        width: 100%;
        text-align: center;
        margin: 20px 0;
    }
`
