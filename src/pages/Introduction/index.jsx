import React from 'react';
import { Page, Title, Logo25, Paragraph } from './styles';
import introduction from '../../lib/assets/mockData/introduction.json';
import VideoCard from '../../components/VideoCard';

export default class AboutAvril extends React.Component {
  render() {
    const legend = "Nous croyons qu`être mieux est tout ce dont nous avons besoin pour bien vivre."
    return (
        <Page>
          <VideoCard legend={legend}/>
            {introduction.data.map(intro => {
                return(
                    <Paragraph>{intro.p1}</Paragraph>
                )})
            }
          <Title>AVRIL. BIEN MIEUX<Logo25 src="images/logo25.png"/></Title>
        </Page>
    );
  }
}