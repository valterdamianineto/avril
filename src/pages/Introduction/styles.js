import styled from "styled-components";

export const Page = styled.div`
    background: #FFFCF5;
    margin-bottom: 30px;
`

export const Title = styled.div`
    width: 90%;
    font-weight: bold;
    letter-spacing: 0.01em;
    font-size: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #373D49;
    margin: 80px auto;
    @media (max-width: 768px) {
        height: 29px;
        font-size: 24px;
      }
`

export const Logo25 = styled.img`
    width: 40px;
    margin-left: 24px;
    
`

export const Paragraph = styled.p`
    font-size: 28px;
    width: 80%;
    margin: 24px auto;
    padding: 18px;
    @media (max-width: 768px) {
        font-size: 15px;
        font-style: normal;
        line-height: 22px;
        letter-spacing: 0.005em;
        text-align: center;
        margin: 8px auto;
    }
`