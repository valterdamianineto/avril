import React from 'react';
import { Page } from './styles';
import SectionCard from '../../components/SectionCard';
import ourStores from '../../lib/assets/mockData/our-stores.json';

const AboutAvril = () =>  {
  function getWindowDimensions() {
    const { innerWidth: width} = window;
    return {
      width
    };
  }
  const screen = getWindowDimensions()
  
  return (
      <Page>
          {ourStores.data.map((store, index) => {
          const mobile = (screen.width < 780 ? true : false) 
          return (
            <SectionCard key={index}
            mobile={mobile}
            side={index % 2 === 0 ? 'left' : 'right'}
            imagePath={store.imagePath}
            p1={store.p1}
            p2={store.p2}
            text={store.btn}/>
          )})}
        </Page>
    );
}

export default AboutAvril;