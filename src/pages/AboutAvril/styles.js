import styled from "styled-components";

export const Page = styled.div`
    background: #FAFBFB;
    margin-bottom: 70px;
    width: 70%;
    margin: auto;
    @media (max-width: 768px) {
        width: 100%;
    }
`
