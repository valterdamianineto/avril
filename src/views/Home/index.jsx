import React from 'react';
import { Container } from './styles';
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import OurStore from '../../pages/OurStores'
import AboutAvril from '../../pages/AboutAvril'
import Introduction from '../../pages/Introduction'
import Program from '../../pages/Program'
import ImageCarousel from '../../components/Carousel'

export default class Home extends React.Component {
  render() {
    return (
      <Container>
          <Header/>
          <Introduction/>
          <AboutAvril/>
          <OurStore/>
          <ImageCarousel/>
          <Program/>
          <Footer/>
      </Container>
    );
  }
}