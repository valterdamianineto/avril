export function createMarkup(content) {
    return {__html: content};
}

export default function HtmlRender(content) {
    return <div dangerouslySetInnerHTML={createMarkup(content)} />;
}
