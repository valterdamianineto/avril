import {
  ApolloClient,
  InMemoryCache
} from "@apollo/client";

const client = new ApolloClient({
  method: 'post',
  uri: 'http://avril.2dev.ca/graphql',
  crossDomain: false,
  mode : 'no-cors',
  headers: {
      "Access-Control-Allow-Origin": "http://localhost:3001",
      "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
      "Access-Control-Allow-Headers": "Origin, Content-Type",
      'Access-Control-Allow-Credentials': 'true',
      "Access-Control-Max-Age" : "200",

  },
  cache: new InMemoryCache(),
});

export default client;
