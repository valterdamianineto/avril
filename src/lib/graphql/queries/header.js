import { gql } from '@apollo/client'
import client  from '../apolloClient'

const GET_HEADER = gql`
  query {
    main(identifiers: "main", menu_id: 2){
      items{
        category_id
        cms_page_identifier
        icon
        icon_active_state
        is_active
        item_class
        item_id
        parent_id
        position
        shop_menu_item
        title
        url
        url_type
      }
    }
  }
`
export default function fetchHeader() {
  return client.query({
    query: GET_HEADER,
  })
}
