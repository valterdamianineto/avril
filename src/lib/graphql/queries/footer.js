import { gql } from '@apollo/client'
import client  from '../apolloClient'

const FOOTER_REINSURANCE = gql`
    query {
      cmsBlocks(identifiers: "footer_reinsurance"){
        items{
          title
          content
          identifier
          disabled
        }
      }
    }   
`

const FOOTER_NL_SOCIAL = gql`
    query {
      cmsBlocks(identifiers: "footer_nl_social"){
        items{
          title
          content
          identifier
          disabled
        }
      }
    }   
`

const FOOTER_INFO = gql`
    query {
      cmsBlocks(identifiers: "footer_info"){
        items{
          title
          content
          identifier
          disabled
        }
      }
    }   
`

const FOOTER_LINKS_ABOUT = gql`
    query {
      cmsBlocks(identifiers: "footer_links_about"){
        items{
          title
          content
          identifier
          disabled
        }
      }
    }   
`

const FOOTER_LINKS_SERVICE_LINKS = gql`
    query {
      cmsBlocks(identifiers: "footer_links_service_links"){
        items{
          title
          content
          identifier
          disabled
        }
      }
    }   
`

const FOOTER_LINKS_NAV = gql`
    query {
      cmsBlocks(identifiers: "footer_links_nav"){
        items{
          title
          content
          identifier
          disabled
        }
      }
    }   
`

const FOOTER_MISC_LINKS = gql`
    query {
      cmsBlocks(identifiers: "footer_misc_links"){
        items{
          title
          content
          identifier
          disabled
        }
      }
    }   
`

export function fetchFooterReinsurance() {
  return client.query({
    query: FOOTER_REINSURANCE,
  })
}

export function fetchFooterNlSocial() {
  return client.query({
    query: FOOTER_NL_SOCIAL,
  })
}

export function fetchFooterInfo() {
  return client.query({
    query: FOOTER_INFO,
  })
}

export function fetchFooterLinksAbout() {
  return client.query({
    query: FOOTER_LINKS_ABOUT,
  })
}

export function fetchFooterServicesLinks() {
  return client.query({
    query: FOOTER_LINKS_SERVICE_LINKS,
  })
}

export function fetchFooterLinksNav() {
  return client.query({
    query: FOOTER_LINKS_NAV,
  })
}

export function fetchFooterMiscLinks() {
  return client.query({
    query: FOOTER_MISC_LINKS,
  })
}