import { gql } from '@apollo/client'
import client  from '../apolloClient'

const CART = gql`
  query ($cart_id_1: String!) {
    cart(cart_id: $cart_id_1) {
      items {
        alley
        items_count
        items {
          id
          product {
            sku
          }
        }
      }
      prices {
        applied_taxes {
          label
          amount {
            value
            currency
          }
        }
      }
      applied_reward_points {
        points
        money {
          currency
          value
        }
      }
    }
  }
`
export default function fetchGetCart() {
  return client.query({
    query: CART,
  })
}
