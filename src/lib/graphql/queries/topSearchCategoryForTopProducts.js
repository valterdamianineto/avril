import { gql } from '@apollo/client'
import client  from '../apolloClient'

const TOP_SEARCH_CATEGORY_FOR_TOP_PRODUCTS_ = gql`
  query {
    getTopSearchCategoryForTopProducts {
      category_id
      name
      url
    }
  }
`
export default function fetchSearchCategoryForTopProductss() {
  return client.query({
    query: TOP_SEARCH_CATEGORY_FOR_TOP_PRODUCTS_,
  })
}
