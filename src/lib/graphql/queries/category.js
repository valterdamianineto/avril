import { gql } from '@apollo/client'
import client  from '../apolloClient'

const CATEGORY = gql`
  query ($id_1: Int!) {
    category(id: $id_1) {
      children {
        id
        url
        name
        include_in_menu
        position
        level
        children {
          id
          url
          name
          include_in_menu
          position
          level
          children {
            id
            url
            name
            include_in_menu
            position
            level
          }
        }
      }
    }
  }  
`
export default function fetchGetCategory() {
  return client.query({
    query: CATEGORY,
  })
}
