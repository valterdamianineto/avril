import { gql } from '@apollo/client'
import client  from '../apolloClient'

const TOP_SEARCH_CATEGORIES = gql`
  query {
    getTopSearchCategories {
      category_id
      name
      url
      position
    }
  }  
`
export default function fetchSearchCategories() {
  return client.query({
    query: TOP_SEARCH_CATEGORIES,
  })
}
