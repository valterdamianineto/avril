import { gql } from '@apollo/client'
import client  from '../apolloClient'

const TOP_SEARCH_PRODUCTS = gql`
  query ($store_code_1: String!, $store_code_2: String!, $store_code_3: String!) {
    getTopSearchProducts {
      product {
        id
        sku
        name
        type_id
        warehouse_stock_status
        brand
        size
        url
        matnr
        special_from_date
        special_to_date
        price_type_kg
        price_per_kgs
        special_price_per_kgs
        fee {
          currency
          value
        }
        shipments(store_code: $store_code_1) {
          is_pickable
          is_saleable
          is_shippable
        }
        price_range {
          minimum_price {
            discount {
              amount_off
              percent_off
            }
            final_price {
              currency
              value
            }
            regular_price {
              currency
              value
            }
          }
        }
        thumbnail {
          path
          url
        }
        ... on CustomizableProductInterface {
          options {
            ... on CustomizableDropDownOption {
              dropdownValues: value {
                option_type_id
                price
                price_type
                sku
                title
                sort_order
              }
            }
            ... on CustomizableRadioOption {
              dropdownValues: value {
                option_type_id
                price
                price_type
                sku
                title
                sort_order
              }
            }
            ... on CustomizableCheckboxOption {
              checkboxValues: value {
                option_type_id
                price
                price_type
                sku
                title
                sort_order
              }
            }
            ... on CustomizableMultipleOption {
              checkboxValues: value {
                option_type_id
                price
                price_type
                sku
                title
                sort_order
              }
            }
            ... on CustomizableFieldOption {
              fieldValues: value {
                price
                price_type
                sku
                max_characters
              }
              product_sku
            }
            ... on CustomizableAreaOption {
              areaValues: value {
                price
                price_type
                sku
                max_characters
              }
              product_sku
            }
            title
            required
            sort_order
            option_id
          }
        }
        small_image {
          path
          url
        }
        price_tiers {
          discount {
            amount_off
            percent_off
          }
          final_price {
            currency
            value
          }
          quantity
        }
        ... on SimpleProduct {
          weight
        }
        is_deliverable
        label {
          title
          description
        }
        attributes: s_attributes {
          attribute_id
          attribute_value
          attribute_code
          attribute_type
          attribute_label
          attribute_options {
            label
            value
            swatch_data {
              type
              value
            }
          }
        }
        url
        ... on ConfigurableProduct {
          configurable_options {
            attribute_code
            values {
              value_index
            }
          }
          variants {
            product {
              id
              sku
              name
              type_id
              warehouse_stock_status
              brand
              size
              url
              matnr
              special_from_date
              special_to_date
              price_type_kg
              price_per_kgs
              special_price_per_kgs
              fee {
                currency
                value
              }
              shipments(store_code: $store_code_2) {
                is_pickable
                is_saleable
                is_shippable
              }
              price_range {
                minimum_price {
                  discount {
                    amount_off
                    percent_off
                  }
                  final_price {
                    currency
                    value
                  }
                  regular_price {
                    currency
                    value
                  }
                }
              }
              thumbnail {
                path
                url
              }
              ... on CustomizableProductInterface {
                options {
                  ... on CustomizableDropDownOption {
                    dropdownValues: value {
                      option_type_id
                      price
                      price_type
                      sku
                      title
                      sort_order
                    }
                  }
                  ... on CustomizableRadioOption {
                    dropdownValues: value {
                      option_type_id
                      price
                      price_type
                      sku
                      title
                      sort_order
                    }
                  }
                  ... on CustomizableCheckboxOption {
                    checkboxValues: value {
                      option_type_id
                      price
                      price_type
                      sku
                      title
                      sort_order
                    }
                  }
                  ... on CustomizableMultipleOption {
                    checkboxValues: value {
                      option_type_id
                      price
                      price_type
                      sku
                      title
                      sort_order
                    }
                  }
                  ... on CustomizableFieldOption {
                    fieldValues: value {
                      price
                      price_type
                      sku
                      max_characters
                    }
                    product_sku
                  }
                  ... on CustomizableAreaOption {
                    areaValues: value {
                      price
                      price_type
                      sku
                      max_characters
                    }
                    product_sku
                  }
                  title
                  required
                  sort_order
                  option_id
                }
              }
              small_image {
                path
                url
              }
              price_tiers {
                discount {
                  amount_off
                  percent_off
                }
                final_price {
                  currency
                  value
                }
                quantity
              }
              ... on SimpleProduct {
                weight
              }
              is_deliverable
              label {
                title
                description
              }
              attributes: s_attributes {
                attribute_id
                attribute_value
                attribute_code
                attribute_type
                attribute_label
              }
            }
          }
        }
        ... on BundleProduct {
          price_view
          dynamic_price
          dynamic_sku
          ship_bundle_items
          dynamic_weight
          items {
            option_id
            title
            required
            type
            position
            sku
            options {
              id
              label
              quantity
              position
              is_default
              price
              price_type
              can_change_quantity
              product {
                id
                sku
                name
                type_id
                warehouse_stock_status
                brand
                size
                url
                matnr
                special_from_date
                special_to_date
                price_type_kg
                price_per_kgs
                special_price_per_kgs
                fee {
                  currency
                  value
                }
                shipments(store_code: $store_code_3) {
                  is_pickable
                  is_saleable
                  is_shippable
                }
                price_range {
                  minimum_price {
                    discount {
                      amount_off
                      percent_off
                    }
                    final_price {
                      currency
                      value
                    }
                    regular_price {
                      currency
                      value
                    }
                  }
                }
                thumbnail {
                  path
                  url
                }
                ... on CustomizableProductInterface {
                  options {
                    ... on CustomizableDropDownOption {
                      dropdownValues: value {
                        option_type_id
                        price
                        price_type
                        sku
                        title
                        sort_order
                      }
                    }
                    ... on CustomizableRadioOption {
                      dropdownValues: value {
                        option_type_id
                        price
                        price_type
                        sku
                        title
                        sort_order
                      }
                    }
                    ... on CustomizableCheckboxOption {
                      checkboxValues: value {
                        option_type_id
                        price
                        price_type
                        sku
                        title
                        sort_order
                      }
                    }
                    ... on CustomizableMultipleOption {
                      checkboxValues: value {
                        option_type_id
                        price
                        price_type
                        sku
                        title
                        sort_order
                      }
                    }
                    ... on CustomizableFieldOption {
                      fieldValues: value {
                        price
                        price_type
                        sku
                        max_characters
                      }
                      product_sku
                    }
                    ... on CustomizableAreaOption {
                      areaValues: value {
                        price
                        price_type
                        sku
                        max_characters
                      }
                      product_sku
                    }
                    title
                    required
                    sort_order
                    option_id
                  }
                }
                small_image {
                  path
                  url
                }
                price_tiers {
                  discount {
                    amount_off
                    percent_off
                  }
                  final_price {
                    currency
                    value
                  }
                  quantity
                }
                ... on SimpleProduct {
                  weight
                }
                is_deliverable
                label {
                  title
                  description
                }
                attributes: s_attributes {
                  attribute_id
                  attribute_value
                  attribute_code
                  attribute_type
                  attribute_label
                }
              }
            }
          }
        }
      }
      position
    }
  }
`
export default function fetchToSearchProducts() {
  return client.query({
    query: TOP_SEARCH_PRODUCTS,
  })
}
