import React from 'react';
import { Container, Title, Li } from './styles';
import Button from '../Button';

const SectionCard = ({title, p1, p2, p3, text}) => {
  return (
    <Container>
      <Title>{title}</Title>
      <Li>{p1}</Li>
      <Li>{p2}</Li>
      <Li>{p3}</Li>
      {text ? 
        <Button width="181px" text={text}/>
        : null
      }
    </Container>
  );
}

export default SectionCard;