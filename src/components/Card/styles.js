import styled from "styled-components";

export const Container = styled.div`
    background: #FAFBFB;
    padding: 1px;
    width: 40%;
    justify-content: center;
    margin: 20px;
    @media (max-width: 768px) {
        display: block;
        width: 90%;
        text-align: center;
        margin: 20px 0;
    }
`

export const Title = styled.p`
    font-size: 24px;
    font-weight: bold;
    color: #373D49;
    width: 100%;
    display: block;
    padding: 4px 4px 4px 50px;
    text-align: left;
    @media (max-width: 768px) {
        text-align: center;
        padding: 4px;
        margin: 20px 0;
    }
`

export const Li = styled.li`
    width: 90%;
    padding: 4px 15px;
    margin: 5px;
    text-align: left;
    ::marker,
    li::marker {
        color: #BBD422 !important;
        font-size: 20px;
    }
`