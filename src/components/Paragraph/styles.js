import styled from "styled-components";

export const Text = styled.p`
    font-size: 15px;
    font-style: normal;
    line-height: 22px;
    letter-spacing: 0.005em;
    text-align: center;
    margin: 25px 13px;
`