import React from 'react';
import { Text } from './styles';

const Paragraph = ({text}) => {
  return (
    <Text>{text}</Text>
  );
}

export default Paragraph;