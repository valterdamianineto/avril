import styled from "styled-components";
import search from "../../lib/assets/icons/search.svg"

export const Container = styled.div`
    height: 50px;
    width: 100%;
    max-width: 600px;
    text-align: center;
    padding: 0px;
    margin: 0px;
    display: block;
    @media (max-width: 768px) {
        display: none;
    }
`

export const SearchBar = styled.div`
    height: 40px;
    text-align: center;
    display: flex;
    border: 2px solid #373d49;
    background: #fafbfb;
    border-radius: 10px;
    &:hover {
    }
    @media (max-width: 768px) {
        border: 0.2px solid #C4C4C4;
    }
`

export const SearchInput = styled.input.attrs({
    placeholder: 'Rechercher des produits'
})`
    width: 95%;
    border: none;
    border-radius: 10px;
    padding: 16px;
    font-size: 14px;
    z-index: 10;
    @media (max-width: 768px) {
        width: 90%;
    }
    &:focus {
        outline: none;
    }
`

export const SearchIcon = styled.img.attrs({
    src: `${search}`
})`
    color: black;
    width: 5%;
    padding: 10px;
    @media (max-width: 768px) {
        width: 10%;
    }
`
export const SearchList = styled.div`
    display: ${({ showModal }) => showModal};
    position: absolute;
    width: 75%;
    height: 80vh;
    background: #fafbfb;
    text-align: left;
    padding: 20px;
    margin: 0% 10% 0% 0%;
    border-radius: 20px;
    z-index: 30;
`
export const Title = styled.p`
    font-size: 24px;
    font-weight: bold;
    color: #373D49;
    width: 100%;
    display: block;
    padding: 4px 4px 4px 50px;
    text-align: left;
    @media (max-width: 768px) {
        text-align: center;
        padding: 4px;
        margin: 20px 0;
    }
`
export const ListItem = styled.p`
    width: 20%;
    font-size: 1rem;
    line-height: 2.4rem;
    position: relative;
    font-weight: 400;
    margin: 0;
    color: #373d49;
    text-align: left;
    cursor: pointer;
    border-right: 1px solid #bbd422;
`

export const Overlay = styled.div`
    position: fixed;
    display: ${({ showModal }) => showModal};
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: transparent;
    z-index: 1;
`
