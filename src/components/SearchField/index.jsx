import React, { useState } from 'react';
import { Container, SearchBar, SearchInput, SearchIcon, SearchList, Overlay, Title, ListItem } from './styles';
import fetchSearchCategories from '../../lib/graphql/queries/topSearchCategories';


const Banner = () => {
  const [searchList, setSearchList] = useState();
  const [showModal, setShowModal] = useState('none')
  async function getData(){
    const response = await fetchSearchCategories()
    return setSearchList(response.data)
  }
  getData()
  

  return (
    <Container>
      <SearchBar onClick={() => setShowModal('block')}>
        <SearchInput/>
        <SearchIcon/>
      </SearchBar>
        <SearchList showModal={showModal}>
          <Title>Catégories</Title>
          {searchList?.getTopSearchCategories.map(item =>
            <ListItem>
            {item.name}
            </ListItem>
          )}
        </SearchList>
        <Overlay showModal={showModal} onClick={() => setShowModal('none')}/>

    </Container>
  );
}

export default Banner;