import React from 'react';

function createMarkup() {
  return {__html: '<div class=\"FooterLinks-About\">\r\n       \r\n        <a href=\"/a-propos\">À propos d`Avril</a>\r\n        <a href=\"/nos-succursales\">Nos succursales</a>\r\n        <!--<a href=\"#\">Événements</a>\r\n        <a href=\"#\">Services offerts</a>-->\r\n        <a href=\"#\">Cartes-cadeaux</a>\r\n        <a href=\"/emplois-et-carrieres\">Emplois et carrières</a>\r\n        <a href=\"/presse\">Presse</a>\r\n    </div>'};
}

const Teste = () =>  {
  return <div dangerouslySetInnerHTML={createMarkup()} />;
}

export default Teste;