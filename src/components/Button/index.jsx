import React from 'react';
import { DefaultButton } from './styles';

const Button = ({ backColor, color, text, padding, width, margin }) => {
    return (
        <DefaultButton 
            backColor={backColor} 
            color={color} 
            padding={padding}
            width={width} 
            margin={margin}
        >
        {text}
        </DefaultButton>
    );
}

export default Button;