import styled from "styled-components";

export const DefaultButton = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 600;
    border-radius: 3px;
    height: 50px;
    text-transform: capitalize;
    border: none;
    font-size: 16px;
    width: ${({ width }) => width ? width : '152px'};
    padding: ${({ padding }) => padding ? padding : '0'};
    color: ${({ color }) => color ? color : '#FFFFFF'};
    background: ${({ backColor }) => backColor ? backColor : '#BBD422'};
    margin: ${({ margin }) => margin ? margin : '30px auto'};
`