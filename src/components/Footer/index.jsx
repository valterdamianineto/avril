import React from 'react';
import { Container } from './styles';
import About from './About'

const Footer = () => {
    return (
      <Container>
        <About/>
      </Container>
    );
}

export default Footer 