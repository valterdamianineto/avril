import styled from "styled-components";
import search from "../../lib/assets/icons/search.svg"

export const Container = styled.div`
    height: 48px;
    width: 100%;
    max-width: 600px;
    text-align: center;
    padding: 0px;
    margin: 0px;
    border: 1px solid #BBD422;
    display: none;
    align-items: center;
    margin: 1px 0 0 0;
    @media (max-width: 768px) {
        display: block;
    }
`

export const SearchBar = styled.div`
    height: 40px;
    text-align: center;
    display: flex;
    background: #fafbfb;
    border: 2px solid ##BBD422;
`

export const SearchInput = styled.input.attrs({
    placeholder: 'Rechercher des produits'
})`
    width: 90%;
    border: none;
    padding: 16px;
    font-size: 14px;
    z-index: 10;
    @media (max-width: 768px) {
        width: 90%;
    }
    &:focus {
        outline: none;
    }
`

export const SearchIcon = styled.img.attrs({
    src: `${search}`
})`
    color: black;
    width: 10%;
    padding: 10px;
    @media (max-width: 768px) {
        width: 10%;
    }
`
export const SearchList = styled.div`
    display: ${({ showModal }) => showModal};
    position: absolute;
    width: 90%;
    height: 80vh;
    background: #fafbfb;
    text-align: left;
    padding: 20px;
    margin: 2.5% 10% 0% 0%;
    z-index: 30;
`

export const TitleBox = styled.div`
    display: flex;
`
export const Title = styled.p`
    font-size: 18px;
    font-weight: bold;
    color: #373D49;
    width: 100%;
    display: block;
    justify
    text-align: left;
    padding: 0px;
    margin: 2px 0;
`

export const Close = styled.p`
    font-size: 24px;
    padding: 0;
    margin: 0;
    color: #373D49;
`

export const ListItem = styled.p`
    width: 100%;
    font-size: 1rem;
    line-height: 2.4rem;
    position: relative;
    font-weight: 400;
    margin: 0;
    color: #373d49;
    text-align: left;
    cursor: pointer;
`
