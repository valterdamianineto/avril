import styled from "styled-components";

export const Img = styled.img.attrs(
    props => ({'src': props.image})
)`
    width: 100%;
    margin: 0;
    padding: 0;
`