import React from 'react';
import { Img } from './styles';

const Image = ({image}) => {
  return (
      <Img image={image}/>
  );
}

export default Image;