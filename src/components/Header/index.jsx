import React, { useState } from 'react';
import Nav from '../SideMenu';
import Navbar from '../Navbar';
import SearchField from '../SearchFieldMobile';
import Banner from '../../components/InfoBanner';
const Header = () => {
  const [open, setOpen] = useState(false)
  return (
    <>
      <Navbar/>
      <Nav open={open}/>
      <SearchField></SearchField>
      <Banner/>
    </>
  );
}

export default Header;