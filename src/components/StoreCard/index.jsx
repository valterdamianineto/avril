import React from 'react';
import { Container, StoreImage, StoreName} from './styles';

const StoreCard = (props) => {
  return (
    <Container>
        <StoreImage 
            image={props.image}
        />
        <StoreName>{props.name}</StoreName>
    </Container>
  );
}

export default StoreCard;