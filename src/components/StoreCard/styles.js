import styled from "styled-components";

export const Container = styled.div`
    width: 25%;
    background: transparent;
    display: inline-block;
    margin: 2%;
    @media (max-width: 768px) {
        background: #F7F8FA;
        width: 94%;
        padding: 1%;
    }
`

export const StoreImage = styled.img.attrs(
    props => ({'src': props.image})
)`
    max-width: 100%;
    @media (max-width: 768px) {
        max-width: 90%;
    }
`
export const StoreName = styled.div`
    display: none;
    @media (max-width: 768px) {
        display: block;
        weight: 600;
        font-size: 16px;
        line-height: 24px;
        letter: 1%;
        align: center;
        text-transform: Capitalize;
    }
`
