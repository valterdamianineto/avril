import styled from "styled-components";

export const Container = styled.div`
    width: 100%;
    text-align: center;
    margin: 20px 0;
    display: flex;
    @media (max-width: 768px) {
        display: block;
        width: 100%;
        text-align: center;
        margin: 20px 0;
    }
`

export const Image = styled.img.attrs(
    props => ({'src': props.image})
)`
    width: 40%;
    margin: 0;
    padding: 0;
    @media (max-width: 768px) {
        width: 100%;
        text-align: center;
        margin: 20px 0;
    }
`

export const ParagraphBox = styled.div`
    width: 30%;
    margin: 0;
    padding: 10%;
    display: flex;
    flex-direction: column;
    align-items: center;
    @media (max-width: 768px) {
        width: 100%;
        text-align: center;
        margin: 20px 0;
        padding: 0;

    }
`

export const Paragraph = styled.p`
    width: 100%;
    margin: 0;
    padding: 20px;
    text-align: left;
    line-height: 150%;
    letter-spacing: 0.05em;
    @media (max-width: 768px) {
        font-size: 16px;
        text-align: center;
        margin: 10px 0;
        width: 90%;
    }
`