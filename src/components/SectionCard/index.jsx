import React from 'react';
import { Container, Image, ParagraphBox, Paragraph } from './styles';
import Button from '../Button';

const SectionCard = ({imagePath, p1, p2, text, side, mobile}) => {
  return (
    <Container>
      {side === 'left' | mobile ?
      <>
      <Image image={imagePath}/>
      <ParagraphBox>
      <Paragraph>{p1}</Paragraph>
      <Paragraph>{p2}</Paragraph>
        {text ? 
          <Button text={text}/>
          : null
        }
      </ParagraphBox> 
      </>:<>
      <ParagraphBox>
      <Paragraph>{p1}</Paragraph>
      <Paragraph>{p2}</Paragraph>
        {text ? 
          <Button text={text}/>
          : null
        }
      </ParagraphBox> 
      <Image image={imagePath}/>
      </> }
    </Container>
  );
}

export default SectionCard;