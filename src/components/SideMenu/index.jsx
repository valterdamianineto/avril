
import React from 'react';
import { Ul } from './styles';

const SideMenu = ({ open }) => {
    return (
        <Ul open={open}>
            <li>Home</li>
            <li>About Us</li>
            <li>Contact Us</li>
            <li>Sign In</li>
            <li>Sign Up</li>
        </Ul>
    )
  }
  
  export default SideMenu