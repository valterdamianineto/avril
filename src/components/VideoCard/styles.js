import styled from 'styled-components';

export const Video = styled.div`
  width: 100%;
  height: 80vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: ${({ backColor }) => backColor ? backColor : '#EAEAEA'};
  @media (max-width: 768px) {
    height: ${({ height }) => height ? height : '210px'};
  }
`

export const VideoButton = styled.img.attrs(
  props => ({'src': props.image})
)`
  width: 80px;
  height: 80px;
  @media (max-width: 768px) {
    width: 45px;
    height: 45px;
  }
`

export const VideoLegend = styled.p`
  padding: 0 15px;
  font-weight: 500;
  font-size: 32px;
  margin-top: 30vh;
  width: 40%;
  position: absolute;
  @media (max-width: 768px) {
    width: 90%;
    font-size: 16px;
    margin-top: 150px;
  }
`
