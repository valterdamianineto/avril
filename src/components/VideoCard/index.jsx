import React from 'react';
import { Video, VideoButton, VideoLegend } from './styles'

const VideoCard = ({width, height, legend}) => {
  return (
    <>
      <Video width={width} height={height}>
        <VideoButton image="images/videoIcon.png"/>
        <VideoLegend>{legend}</VideoLegend>
      </Video>
    </>
  )
}

export default VideoCard