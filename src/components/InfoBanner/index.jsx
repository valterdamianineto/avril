import React, { useState } from 'react';
import { Container, Message, StyledLink, CloseButton } from './styles';

const Banner = () => {
  const [showBanner, setShowBanner] = useState();
  
  return (
    <Container display={showBanner} >
      <Message>
        covid-19 : informations importantes pour nos clients,
        <StyledLink>
           en savoir plus
        </StyledLink>
      </Message>
      <CloseButton onClick={() => setShowBanner('none')}>X</CloseButton>
    </Container>
  );
}

export default Banner;