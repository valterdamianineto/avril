import styled from "styled-components";

export const Container = styled.div`
    background: #BBD422;
    padding: 10px;
    text-align: center;
    display: ${({ display }) => display ? display : 'flex'};;
    align-items: center;
`

export const Message = styled.p`
    width:100%;
    padding: 0;
    margin: 0;
    font-weight: 400;
    font-size: 14px;
    text-transform: uppercase;
`
export const StyledLink = styled.a`
    padding: 0 4px;
    margin: 0;
    text-transform: uppercase;
    text-decoration: underline;
    font-weight: 500;
`

export const CloseButton = styled.button`
    padding: 5px;
    margin: 0 5px;
    font-size: 20px;
    background: transparent;
    border: none;
    cursor: pointer;
`