import React from "react";
import { Carousel } from "react-responsive-carousel";
import { CarouselBox, Title, Card, Img } from './styles'

const ImageCarousel = () => {
    return (
        <CarouselBox>
            <Title>AVRIL. BIEN MIEUX : Témoignages</Title>
            <Carousel autoPlay showThumbs={false} showArrows={true}>
                <Card>
                    <Img alt="laval" src="images/laval.png" />
                </Card>
                <Card>
                    <Img alt="quebec" src="images/quebec.png" />
                </Card>
                <Card>
                    <Img alt="levis" src="images/levis.png" />
                </Card>
                <Card>
                    <Img alt="brossard" src="images/brossard.png" />
                </Card>
                <Card>
                    <Img alt="saintBruno" src="images/saintBruno.png" />
                </Card>
                <Card>
                    <Img alt="sherbrooke" src="images/sherbrooke.png" />
                </Card>
                <Card>
                    <Img alt="longueil" src="images/longueil.png" />
                </Card>
            </Carousel>
        </CarouselBox>
)}

export default ImageCarousel
