import styled from "styled-components";

export const  CarouselBox = styled.div`
    background: #FFFCF5;
    margin: 30px 0 0 0;
    padding: 20px 0 0 0;
    div {
        div.carousel {
            div.thumbs-wrapper{
                height: 0px;
                margin: 0;
                padding: 0;
            }
        }
        padding: 0px;
        width: 60%;
        margin: 20px auto;
        p.carousel-status{
            display: none;
        }
        div {
            ul.control-dots {
                height: 30px;
                padding: 50;
                li.dot {
                    opacity: 0.5;
                    background-color: #CEE15A;
                    padding: 7px;
                }
            }
        }
        @media (max-width: 768px) {
            padding: 0px;
            margin: 0;
            width: 96%;
            margin: 2%;
            div {
                ul.control-dots {
                    li.dot {
                        padding: 1px;
                    }
                }
            }
        }
    }
`
export const Title = styled.div`
    font-style: normal;
    font-weight: bold;
    color: #373D49;
    align-items: center;
    font-size: 40px;
    letter-spacing: 0.01em;
    padding: 50px;
    margin: 500px;
    @media (max-width: 768px) {
        font-size: 24px;
        letter-spacing: 0.01em;
        padding: 50px;
        margin: 50px;
    }
`

export const Card = styled.div`
    background: #FAFBFB;
    margin: 15px 0;
    padding: 1px;
`

export const Img = styled.img`
    font-size: 24px;
    font-weight: bold;
    color: #373D49;
`
