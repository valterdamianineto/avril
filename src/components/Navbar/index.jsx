import React from 'react';
import Burger from '../Burger';
import { Nav, Logo, LogoMobile, IconBox, Cart, Pointer } from './styles';
import SearchField from '../SearchField';

const Navbar = () => {
  return (
    <Nav>
      <Logo/>
      <LogoMobile/>
      <SearchField/>
      <IconBox>
        <Pointer/>
        <Cart/>
        <Burger />
      </IconBox>
    </Nav>
  )
}

export default Navbar