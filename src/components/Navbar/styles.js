import styled from 'styled-components';
import logoSmall from '../../lib/assets/icons/logoSmall.svg'
import logo from '../../lib/assets/icons/logo.svg'
import cart from '../../lib/assets/icons/cart.svg'
import pointer from '../../lib/assets/icons/pointer.svg'

export const Nav = styled.nav`
    background: #373D49;
    width: 100%;
    height: 103px;
    border-bottom: none;
    position: relative;
    padding: 10px 0px;
    display: flex;
    justify-content: left;
    align-items: center;
    @media (max-width: 768px) {
        justify-content: right;
        height: 38px;
    }
`

export const IconBox = styled.div`
    width: 80%;
    margin: 0 1%;
    display: flex;
    justify-content: end;
    @media (max-width: 768px) {
        align-items: center;
        height: 38px;
    }
`

export const Logo = styled.img.attrs({
    src: `${logo}`
})`
    width:121px;
    height: 72px;
    padding: 15px
    display: block;
    margin: 0 50px;
    @media (max-width: 768px) {
        display: none;
    }
`

export const LogoMobile = styled.img.attrs({
    src: `${logoSmall}`
})`
    width: 60px;
    padding: 15px;
    display: none;
    @media (max-width: 768px) {
        display: block;
    }
`

export const Pointer = styled.img.attrs({
    src: `${pointer}`
})`
    width: 20px;
    position: relative;
    right: 0vh;
    z-index: 20;
    display: none;
    @media (max-width: 768px) {
        display: flex;
        margin: 0 10px;
    }
`

export const Cart = styled.img.attrs({
    src: `${cart}`
})`
    width: 30px;
    position: relative;
    right: 0vh;
    z-index: 20;
    display: none;
    @media (max-width: 768px) {
        display: flex;
        margin: 0 10px;
    }
`
