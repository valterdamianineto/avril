# Avril Supermarché Santé 

<p align="center" fontSize="60px">
  Avril Landing Page
</p>


## Project

The Avril  app, was built using the React framework, based on JavaScript, the application reproduces an institutional page of a supermarket chain

Data displayed in this app was consumed from JSON files saved in the app or via GraphQl


##  Technologies

-  [JavaScript](https://www.javascript.com/)
-  [React](https://pt-br.reactjs.org/)
-  [Styled Components](https://styled-components.com/)
-  [Apollo Client](https://www.apollographql.com/)
-  [React Responsive Carousel](https://react-responsive-carousel.js.org/)


## 📥 Installation and execution

Clone this repository and access the directory.

```bash
$ git git@gitlab.com:valterdamianineto/avril.git && cd avril
```

```bash
# To run this project it is necessary to have Node installed, as well as Yarn or NPM

# Installing dependencies
$ npm i

# Running application
$ npm start

# The system default execution port is:
$ http://localhost:3000/

```
